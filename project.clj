(defproject codox-theme-dark "0.1.1"
  :description "A dark version of the default Codox theme"
  :url "https://gitlab.com/docmenthol/codox-theme-dark"
  :scm {:name "git" :url "https://gitlab.com/docmenthol/codox-theme-dark"}
  :license {:name "Eclipse Public License 1.0"
            :url "https://www.eclipse.org/legal/epl-v10.html"
            :year 2010
            :key "epl-1.0"})
