# codox-theme-dark

A work-in-progress dark version of the default Codox theme.

## Screenshots

![Main page](screenshots/01.png)

![Sample namespace](screenshots/02.png)

## Usage

Add the dependency to your `project.clj`:

[![Clojars Project](https://img.shields.io/clojars/v/codox-theme-dark.svg)](https://clojars.org/codox-theme-dark)

Then configure `codox` with the theme:

```clojure
:codox {:themes [:dark]}
```

Enable Markdown for syntax highlighting.

```clojure
:codox {:metadata {:doc/formt :markdown}
        :themes [:dark]}
```

## Derivative

This theme is based on the Codox default assets license under the [Eclipse Public License v1.0](http://www.eclipse.org/legal/epl-v10.html).
